const webpack = require('webpack')

module.exports = {
  /*
  ** Headers of the page
  */
    modules: [
    [
      '@nuxtjs/yandex-metrika',
      {
        id: '24171493',
        webvisor: true,
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
      }
    ],
    ['@nuxtjs/google-adsense', {
      id: 'ca-pub-6835904328811404'
    }],
    ['@nuxtjs/google-analytics', {
      id: 'UA-49805518-1',
    }]
  ],
  env:{
    baseUrl: process.env.BASE_URL || 'http://api.monkeydevstudio.ru/',
    baseApi: process.env.BASE_API || 'http://api.monkeydevstudio.ru/',
    HOST_URL: process.env.HOST_URL || 'http://api.monkeydevstudio.ru/',
  },
  head: {
    title: 'epictype',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { name: 'verify-admitad', content: '9a38cf51fc' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' },
    ],
    script:[
      {src:'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js'},
      {src:'https://apis.google.com/js/platform.js'},
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css' },
      { rel: 'stylesheet', href: '/font-awesome.min.css' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  plugins:['~plugins/axios.js','~plugins/bootstrap.js','~plugins/popper.js'],
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    vendor: ['jquery','bootstrap','popper.js'],
    plugins: [
      // set shortcuts as global for bootstrap
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        "window.jQuery": "jquery",
        "Tether": 'tether'
      })
    ],
    /*
    extend (config, ctx) {
      if (ctx.dev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }*/
  }
}
